package com.crabi.challenge.application.countries

import androidx.lifecycle.*
import com.crabi.challenge.data.api.CountryRemoteRepository
import com.crabi.challenge.data.model.CountryData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.*

@FlowPreview
@ExperimentalCoroutinesApi
class CountriesViewModel(private val repository: CountryRemoteRepository) : ViewModel() {

    val countryFilterRegion = ConflatedBroadcastChannel<Region>()

    val countryList: LiveData<List<CountryData>> = countryFilterRegion.asFlow()
        .flatMapLatest { region ->
            when (region) {
                Region.ALL -> repository.getAllCountries()
                else -> repository.getCountriesForRegion(region.name)
            }
        }
        .catch { println(it.message) }
        .asLiveData()

    init {
        countryFilterRegion.offer(Region.ALL)
    }

    enum class Region {
        EU,
        NAFTA,
        ALL
    }
}