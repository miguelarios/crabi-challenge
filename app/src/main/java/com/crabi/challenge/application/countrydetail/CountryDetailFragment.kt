package com.crabi.challenge.application.countrydetail


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.crabi.challenge.BR
import com.crabi.challenge.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_country_detail.*

class CountryDetailFragment : Fragment(), OnMapReadyCallback {

    private val args: CountryDetailFragmentArgs by navArgs()
    private lateinit var mMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_country_detail, container, false)
        binding.setVariable(BR.country, args.country)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        recycler_view.apply {
            adapter = LanguagesAdapter(args.country.languages)
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val coord = LatLng(args.country.latlng[0], args.country.latlng[1])
        mMap.addMarker(MarkerOptions().position(coord).title(args.country.name))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(coord))
    }
}
