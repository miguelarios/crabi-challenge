package com.crabi.challenge.application

import android.app.Application
import com.crabi.challenge.application.countries.CountriesViewModel
import com.crabi.challenge.data.api.CountryRemoteRepository
import com.crabi.challenge.data.api.CountryService
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            androidLogger(Level.DEBUG)
            modules(appModule)
        }
    }

    private val appModule = module {
        single {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://restcountries.eu/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return@single retrofit.create(CountryService::class.java)
        }

        single { CountryRemoteRepository(get()) }

        viewModel { CountriesViewModel(get()) }
    }
}