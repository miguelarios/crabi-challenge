package com.crabi.challenge.application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.crabi.challenge.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
