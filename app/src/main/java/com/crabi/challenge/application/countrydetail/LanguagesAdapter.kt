package com.crabi.challenge.application.countrydetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.crabi.challenge.data.model.CountryData

class LanguagesAdapter(private val data: List<CountryData.Language>): RecyclerView.Adapter<LanguagesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_1, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position].name)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(string: String) {
            (itemView as TextView).text = string
        }
    }
}