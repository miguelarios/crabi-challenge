package com.crabi.challenge.application.countries

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.crabi.challenge.application.common.GlideApp
import com.crabi.challenge.application.common.SvgSoftwareLayerSetter
import com.crabi.challenge.data.model.CountryData
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_country.view.*


class CountryAdapter(context: Context, private val clickListener: (CountryData) -> Unit): ListAdapter<CountryData, CountryAdapter.CountryViewHolder>(Callback()) {

    val requestBuilder: RequestBuilder<PictureDrawable>

    init {
        requestBuilder = GlideApp.with(context)
            .`as`(PictureDrawable::class.java)
            .transition(withCrossFade())
            .listener(SvgSoftwareLayerSetter())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        return CountryViewHolder(LayoutInflater.from(parent.context).inflate(com.crabi.challenge.R.layout.row_country, parent, false))
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        holder.bind(getItem(position), requestBuilder, clickListener)
    }

    class CountryViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(country: CountryData, requestBuilder: RequestBuilder<PictureDrawable>, clickListener: (CountryData) -> Unit) {
            itemView.name.text = country.name
            itemView.population.text = country.population.toString()
            requestBuilder.load(country.flagUrl).into(itemView.imageView)
            itemView.setOnClickListener {
                clickListener(country)
            }
        }
    }

    class Callback: DiffUtil.ItemCallback<CountryData>(){
        override fun areItemsTheSame(oldItem: CountryData, newItem: CountryData): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: CountryData, newItem: CountryData): Boolean {
            return oldItem == newItem
        }
    }
}