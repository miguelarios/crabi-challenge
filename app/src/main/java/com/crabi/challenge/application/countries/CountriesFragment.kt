package com.crabi.challenge.application.countries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import com.crabi.challenge.R
import com.crabi.challenge.data.model.CountryData
import kotlinx.android.synthetic.main.fragment_countries.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.random.Random

@FlowPreview
@ExperimentalCoroutinesApi
class CountriesFragment : Fragment() {

    private val countriesViewModel: CountriesViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        countriesViewModel.countryList.observe(this, Observer<List<CountryData>> {
            if (it != null) {
                (recycler_view.adapter as ListAdapter<CountryData, *>).submitList(it)
            }
        })
        return inflater.inflate(R.layout.fragment_countries, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.apply {
            adapter = CountryAdapter(context, this@CountriesFragment::onCountryClicked)
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
        fab.setOnClickListener {
            countriesViewModel.countryFilterRegion.offer(
                CountriesViewModel.Region.values()[Random.nextInt(
                    CountriesViewModel.Region.values().size
                )]
            )
        }
    }

    private fun onCountryClicked(country: CountryData) {
        val action =
            CountriesFragmentDirections.actionCountriesFragmentToCountryDetailFragment(country)
        findNavController().navigate(action)
    }
}
