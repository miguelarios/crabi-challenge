package com.crabi.challenge.domain

import com.crabi.challenge.data.model.CountryData
import kotlinx.coroutines.flow.Flow

interface CountryRepository {
    fun getAllCountries(): Flow<List<CountryData>>

    fun getCountriesForRegion(region: String): Flow<List<CountryData>>
}