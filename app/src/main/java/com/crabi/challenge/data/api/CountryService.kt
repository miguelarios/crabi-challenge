package com.crabi.challenge.data.api

import com.crabi.challenge.data.model.CountryData
import retrofit2.http.GET
import retrofit2.http.Path

interface CountryService {
    @GET("rest/v2/all")
    suspend fun getAllCountries(): List<CountryData>

    @GET("rest/v2/regionalbloc/{regionalbloc}")
    suspend fun getCountriesForRegion(@Path ("regionalbloc") region: String): List<CountryData>
}