package com.crabi.challenge.data.api

import com.crabi.challenge.data.model.CountryData
import com.crabi.challenge.domain.CountryRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CountryRemoteRepository(private val service: CountryService): CountryRepository {

    override fun getAllCountries(): Flow<List<CountryData>> = flow {
        emit(service.getAllCountries())
    }

    override fun getCountriesForRegion(region: String): Flow<List<CountryData>> = flow {
        emit(service.getCountriesForRegion(region))
    }

}