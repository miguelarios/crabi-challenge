package com.crabi.challenge.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.util.*

class CountryData() : Parcelable{

    class Language{
        @SerializedName("name")
        lateinit var name: String

        override fun toString(): String {
            return "Language(name='$name')"
        }

    }

    @SerializedName("name")
    lateinit var name: String
    @SerializedName("population")
    val population: Long = 0
    @SerializedName("flag")
    lateinit var flagUrl: String
    @SerializedName("capital")
    lateinit var capital: String
    @SerializedName("region")
    lateinit var region: String
    @SerializedName("languages")
    lateinit var languages: List<Language>
    @SerializedName("latlng")
    lateinit var latlng: Array<Double>

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        flagUrl = parcel.readString()
        capital = parcel.readString()
        region = parcel.readString()
    }


    override fun toString(): String {
        return "CountryData(name='$name', population=$population, flagUrl='$flagUrl', capital='$capital', region='$region', languages=$languages, latlng=${Arrays.toString(
            latlng
        )})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CountryData

        if (name != other.name) return false
        if (population != other.population) return false
        if (flagUrl != other.flagUrl) return false
        if (capital != other.capital) return false
        if (region != other.region) return false
        if (languages != other.languages) return false
        if (!latlng.contentEquals(other.latlng)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + population.hashCode()
        result = 31 * result + flagUrl.hashCode()
        result = 31 * result + capital.hashCode()
        result = 31 * result + region.hashCode()
        result = 31 * result + languages.hashCode()
        result = 31 * result + latlng.contentHashCode()
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(flagUrl)
        parcel.writeString(capital)
        parcel.writeString(region)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CountryData> {
        override fun createFromParcel(parcel: Parcel): CountryData {
            return CountryData(parcel)
        }

        override fun newArray(size: Int): Array<CountryData?> {
            return arrayOfNulls(size)
        }
    }
}